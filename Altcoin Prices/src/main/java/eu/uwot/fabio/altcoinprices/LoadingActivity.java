package eu.uwot.fabio.altcoinprices;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

public class LoadingActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Coin coin;
    private ProgressBar progressBar;
    private int progressBarSlotTime;
    private int progressBarProgess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        progressBar = findViewById(R.id.progressBar);

        Thread retrieveData = new Thread() {
            public void run() {
                setPortfolioData();

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        };

        retrieveData.start();
    }

    private void setPortfolioData () {
        prefs = getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();
        coin = new Coin(getApplicationContext(), false);

        int portfolioItems = 0;
        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                portfolioItems += 1;
            }
        }

        progressBarSlotTime = 100 / (portfolioItems + 11);
        progressBarProgess = 0;

        initTradingPair();

        progressBar.setProgress(progressBarProgess += progressBarSlotTime);

        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");
                float currentUnitValue = coin.getCoinValue(coin.coins[i], altcoinCurrency, 0L);
                editor.putFloat(coin.coins[i] + "_currentUnitValue", currentUnitValue);

                float lastDayUnitValue = coin.getCoinValue(coin.coins[i], altcoinCurrency, new UnixTimestamp().getYesterdayUnixTimestamp());
                editor.putFloat(coin.coins[i] + "_lastDayUnitValue", lastDayUnitValue);

                float lastDayUnitValue_btc, currentUnitValue_btc;
                if (coin.coins[i].equals("BTC")) {
                    currentUnitValue_btc = 1f;
                    lastDayUnitValue_btc = 1f;
                } else {
                    currentUnitValue_btc = coin.getCoinValue(coin.coins[i], "BTC", 0L);
                    lastDayUnitValue_btc = coin.getCoinValue(coin.coins[i], "BTC", new UnixTimestamp().getYesterdayUnixTimestamp());
                }

                editor.putFloat(coin.coins[i] + "_currentUnitValue_btc", currentUnitValue_btc);
                editor.putFloat(coin.coins[i] + "_lastDayUnitValue_btc", lastDayUnitValue_btc);
                editor.apply();

                progressBarProgess += progressBarSlotTime;
                progressBar.setProgress(progressBarProgess);
            }
        }
    }

    private void initTradingPair() {
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcusd = coin.getCoinValue("BTC", "USD", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btceur = coin.getCoinValue("BTC", "EUR", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcgbp = coin.getCoinValue("BTC", "GBP", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btccny = coin.getCoinValue("BTC", "CNY", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcjpy = coin.getCoinValue("BTC", "JPY", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcrub = coin.getCoinValue("BTC", "RUB", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btccad = coin.getCoinValue("BTC", "CAD", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcaud = coin.getCoinValue("BTC", "AUD", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcinr = coin.getCoinValue("BTC", "INR", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btckrw = coin.getCoinValue("BTC", "KRW", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcchf = coin.getCoinValue("BTC", "CHF", 0L);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        editor.putFloat("btcusd", btcusd);
        editor.putFloat("btceur", btceur);
        editor.putFloat("btcgbp", btcgbp);
        editor.putFloat("btccny", btccny);
        editor.putFloat("btcjpy", btcjpy);
        editor.putFloat("btcrub", btcrub);
        editor.putFloat("btccad", btccad);
        editor.putFloat("btcaud", btcaud);
        editor.putFloat("btcinr", btcinr);
        editor.putFloat("btckrw", btckrw);
        editor.putFloat("btcchf", btcchf);
        editor.apply();
    }

}
